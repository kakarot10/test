export const loadingTestsBegin = () => {
    return {
        type: 'LOADING_TESTS_BEGIN'
    };
}

export const loadingTestsSuccess = (tests) => {
    return {
        type: 'LOADING_TESTS_SUCCESS',
        tests
    };
}

export const showTests = () => {
    return {
        type: 'SHOW_TESTS'
    };
}

export const showTestDetails = () => {
    return {
        type: 'SHOW_TEST_DETAILS'
    };
}

export const showAddTest = () => {
    return {
        type: 'SHOW_ADD_TEST'
    };
}

export const showEditSection = () => {
    return {
        type: 'SHOW_EDIT_SECTION'
    };
}

export const showEditTest = () => {
    return {
        type: 'SHOW_EDIT_TEST'
    };
}

export const showTestDetailsBegin = (testId) => {
    return {
        type: 'SHOW_TEST_DETAILS_BEGIN',
        testId
    };
}

export const showTestDetailsSuccess = (index, test) => {
    return {
        type: 'SHOW_TEST_DETAILS_SUCCESS',
        index,
        test
    };
}

export const changeTestStatus = (isActive) => {
    return {
        type: 'CHANGE_TEST_STATUS',
        isActive
    };
}

export const changeTestName = (name) => {
    return {
        type: 'CHANGE_TEST_NAME',
        name
    };
}

export const activateTest = () => {
    return {
        type: 'ACTIVATE_TEST'
    };
}

export const deactivateTest = () => {
    return {
        type: 'DEACTIVATE_TEST'
    };
}

export const loadingSectionsBegin = () => {
    return {
        type: 'LOADING_SECTIONS_BEGIN'
    };
}

export const loadingSectionsSuccess = (sections) => {
    return {
        type: 'LOADING_SECTIONS_SUCCESS',
        sections
    };
}

export const showAddSection = () => {
    return {
        type: 'SHOW_ADD_SECTION'
    };
}

export const showSectionDetailsBegin = (sectionId) => {
    return {
        type: 'SHOW_SECTION_DETAILS_BEGIN',
        sectionId
    };
}

export const showSectionDetailsSuccess = (sectionIndex, section) => {
    return {
        type: 'SHOW_SECTION_DETAILS_SUCCESS',
        sectionIndex,
        section
    };
}

export const changeSectionName = (name) => {
    return {
        type: 'CHANGE_SECTION_NAME',
        name
    };
}

export const showSectionDetails = () => {
    return {
        type: 'SHOW_SECTION_DETAILS'
    };
}

export const showEditQuestions = () => {
    return {
        type: 'SHOW_EDIT_QUESTIONS'
    };
}

export const addQuestions = (questions) => {
    return {
        type: 'ADD_QUESTIONS',
        questions
    };
}

export const editQuestion = () => {
    return {
        type: 'EDIT_QUESTION'
    };
}

export const changeQuestionHtml = (questionHtml) => {
    return {
        type: 'CHANGE_QUESTION_HTML',
        questionHtml
    };
}

export const addChoice = (choiceHtml) => {
    return {
        type: 'ADD_CHOICE',
        choiceHtml
    };
}

export const incrementNoOfChoices = (noOfChoices) => {
    return {
        type: 'INCREMENT_NO_OF_CHOICES',
        noOfChoices: noOfChoices+1
    }
}

export const toggleLoginPanelVisibility = () => {
    return {
        type: 'TOGGLE_LOGIN_PANEL_VISIBILITY'
    };
}

export const login = (user) => {
    return {
        type: 'LOGIN',
        user
    };
}

export const logout = () => {
    return {
        type: 'LOGOUT'
    };
}
