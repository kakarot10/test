const initialState = {
    user: null,
    loginPanelVisibility: 'block',
    currentView: 'TESTS_LIST',
    testsList: [],
    sectionsList: [],
    questionsList: [],
    testGettingEdited: {},
    userId: 1,
    editQuestions: false,
    choices: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOADING_TESTS_BEGIN':
            return Object.assign({}, state, {
                currentView: 'TESTS_LIST',
                isTestsLoading: true
            });
        case 'TOGGLE_LOGIN_PANEL_VISIBILITY':
            let loginPanelVisibility = state.loginPanelVisibility;
            if(loginPanelVisibility === 'none') {
                  return Object.assign({}, state, {
                    loginPanelVisibility: 'block'
                  });
            } else {
                  return Object.assign({}, state, {
                    loginPanelVisibility: 'none'
                  });
            }
        case 'LOGIN':
            return Object.assign({}, state, {
                user: action.user
            });
        case 'LOGOUT':
            return Object.assign({}, state, {
                user: null
            });
        case 'LOADING_TESTS_SUCCESS':
            return Object.assign({}, state, {
                isTestsLoading: false,
                testsList: action.tests
            });
        case 'SHOW_TESTS':
            return Object.assign({}, state, {
                currentView: 'TESTS_LIST'
            });
        case 'SHOW_TEST_DETAILS':
            return Object.assign({}, state, {
                currentView: 'TEST_DETAILS'
            });
        case 'SHOW_ADD_TEST':
            return Object.assign({}, state, {
                currentView: 'ADD_TEST',
                testStatus: undefined
            });
        case 'SHOW_TEST_DETAILS_BEGIN':
            return Object.assign({}, state, {
                isTestLoading: true
                //currentTestId: action.testId
            });
        case 'SHOW_TEST_DETAILS_SUCCESS':
            return Object.assign({}, state, {
                isTestLoading: false,
                currentView: 'TEST_DETAILS',
                testGettingEdited: action.test,
                currentTestIndex: action.index
            });
        case 'CHANGE_TEST_STATUS':
            let testGettingEdited = Object.assign({}, state.testGettingEdited, {
                is_active: action.isActive
            });
            let testsList = [...state.testsList];
            testsList.splice(
                state.currentTestIndex,
                1,
                testGettingEdited);
            return Object.assign({}, state, {
                testsList: testsList,
                testGettingEdited: testGettingEdited
            });
        case 'CHANGE_TEST_NAME':
            let testGettingEdited1 = Object.assign({}, state.testGettingEdited, {
                name: action.name
            });
            let testsList1 = [...state.testsList];
            testsList1.splice(
                state.currentTestIndex,
                1,
                testGettingEdited1);
            return Object.assign({}, state, {
                testsList: testsList1,
                testGettingEdited: testGettingEdited1
            });
        case 'SHOW_EDIT_TEST':
            return Object.assign({}, state, {
                currentView: 'EDIT_TEST',
                currentViewInEditTest: 'EDIT_TEST'
            });
        case 'SHOW_EDIT_SECTION':
            return Object.assign({}, state, {
                currentViewInEditTest: 'EDIT_SECTION',
                currentViewInEditSection: ''
            });
        case 'ACTIVATE_TEST':
            return Object.assign({}, state, {
                testStatus: true
            });
        case 'DEACTIVATE_TEST':
            return Object.assign({}, state, {
                testStatus: false
            });
        case 'LOADING_SECTIONS_BEGIN':
            return Object.assign({}, state, {
                isSectionsLoading: true,
                currentViewInEditSection: ''
            });
        case 'LOADING_SECTIONS_SUCCESS':
            return Object.assign({}, state, {
                isSectionsLoading: false,
                sectionsList: action.sections
            });
        case 'SHOW_ADD_SECTION':
            return Object.assign({}, state, {
                currentViewInEditSection: 'ADD_SECTION'
            });
        case 'SHOW_SECTION_DETAILS':
            return Object.assign({}, state, {
                currentViewInEditTest: 'SECTION_DETAILS',
                editQuestions: false
            });
        case 'SHOW_SECTION_DETAILS_BEGIN':
            return Object.assign({}, state, {
                isSectionsLoading: true
            });
        case 'SHOW_SECTION_DETAILS_SUCCESS':
            return Object.assign({}, state, {
                isSectionsLoading: false,
                currentViewInEditTest: 'SECTION_DETAILS',
                sectionGettingEdited: action.section,
                currentSectionIndex: action.sectionIndex
            });
        case 'CHANGE_SECTION_NAME':
            let sectionGettingEdited1 = Object.assign({}, state.sectionGettingEdited, {
                name: action.name
            });
            let sectionsList1 = [...state.sectionsList];
            sectionsList1.splice(
                state.currentSectionIndex,
                1,
                sectionGettingEdited1);
            return Object.assign({}, state, {
                sectionsList: sectionsList1,
                sectionGettingEdited: sectionGettingEdited1
            });
        case 'SHOW_EDIT_QUESTIONS':
            return Object.assign({}, state, {
                editQuestions: true,
                currentViewInQuestions: 'SHOW_QUESTIONS'
            });
        case 'ADD_QUESTIONS':
            return Object.assign({}, state, {
                questionsList: action.questions
            });
        case 'EDIT_QUESTION':
            return Object.assign({}, state, {
                currentViewInQuestions: 'EDIT_QUESTION',
                noOfChoices: 1
            });
        case 'CHANGE_QUESTION_HTML':
            return Object.assign({}, state, {
                questionHtml: action.questionHtml
            });
        case 'ADD_CHOICE':
            let choices1 = [...state.choices];
            let choice = {
                html: action.choiceHtml,
                is_correct: false
            };
            choices1.push(choice);
            return Object.assign({}, state, {
                choices: [{
                    'html': action.choiceHtml,
                    'is_correct': false
                }]
            });
        case 'INCREMENT_NO_OF_CHOICES':
            return Object.assign({}, state, {
                noOfChoices: action.noOfChoices
            });
        default:
            return state;
    }
};
