import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class TestOption extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.showTestDetailsBegin(this.props.id);
        $.get('/admin/tests/' + this.props.id, (test) => {
            this.props.showTestDetailsSuccess(this.props.index, test);
        });
    }

    render() {
        var className = 'test-option ' + this.props.isActive;
        return (
            <div className={className} onClick={this.handleClick}>{this.props.name}</div>
        );
    }
}

TestOption.propTypes = {
    index: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired
};

export default TestOption;
