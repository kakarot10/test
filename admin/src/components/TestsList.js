import React, {PropTypes, Component} from 'react';
import TestOption from './TestOption';
import Payments from './Payments';
import $ from 'jquery';

var cols = [
    { key: 'activated', label: 'Active' },
    { key: 'id', label: 'ID' },
    { key: 'txn_id', label: 'Txn_ID' },
    { key: 'txn_value', label: 'Txn_value' },
    { key: 'userId', label: 'User ID' },
    { key: 'promoCode', label: 'Promo Code'},
    { key: 'action', label: 'Action'}
];

var promocols = [
    { key: 'promoCode', label: 'Promo Code' },
    { key: 'promoValue', label: 'Value' },
    { key: 'usage', label: 'Usage' },
    { key: 'maxUsage', label: 'Limit' },
    { key: 'valid', label: 'Valid' },
    { key: 'multipleUsage', label: 'Multi Use'}
];

let data = [];
let promodata = [];

class TestsList extends Component {
    constructor(props) {
        super(props);
        this.state = {togl : 1};
        this.addTest = this.addTest.bind(this);
        this.getPayment = this.getPayment.bind(this);
        this.handleTest = this.handleTest.bind(this);
        this.getTests = this.getTests.bind(this);
        this.getPaymentPage = this.getPaymentPage.bind(this);
    }

    addTest() {
        this.props.showAddTest();
    }

    handleTest(e) {
        
        $.ajax({
            url: '/activate_paytm_test/'  + parseInt(e),
            type: 'POST'
        }).then((data) => {
            if(data.success)
                alert(data.message + ".\nPlease REFRESH for the changes to reflect")
        }).then(() => {
             this.getPayment();
        });
    }

    getPayment() {
            
        $.get('/recorded_tests' , (res_obj) => {
            data = [];
            for(var i=0;i<res_obj.length;i++){
                var obj={};

                obj['activated'] = (parseInt(res_obj[i].activated) ? "Yes" : "No");
                obj['id'] = res_obj[i].id;
                obj['txn_id'] = res_obj[i].txn_id;
                obj['txn_value'] = res_obj[i].txn_value;
                obj['userId'] = res_obj[i].userId;
                obj['promoCode'] = res_obj[i].promoCode;
                obj['action'] = <button id={res_obj[i].id} className="test-details-button activate adj" onClick={() => this.handleTest(obj['id'])}>Activate</button>;
                data.push(obj);
            }
        });      

        $.get('/promo_codes' , (res_obj) => {
            promodata = [];
            for(var i=0;i<res_obj.length;i++){
                var obj={};

                obj['promoCode'] = res_obj[i].promoCode;//
                obj['promoValue'] = res_obj[i].promoValue;
                obj['usage'] = (parseInt(res_obj[i].usage) ? "Yes" : "No");
                obj['maxUsage'] = res_obj[i].maxUsage;
                obj['valid'] = (parseInt(res_obj[i].valid) ? "Valid" : "Invalid");
                obj['multipleUsage'] = (parseInt(res_obj[i].multipleUsage) ? "Yes" : "No");
                promodata.push(obj);
            }
        });

    }

    getPaymentPage() {
        this.setState({togl: 2});
        this.getPayment();
    }

    getTests() {
        this.setState({togl: 1});
    }

    render() {
        return (
            <div className="tests-list">
             {(this.state.togl !== 1 ) ? <button className="add-test-button" onClick={this.getTests}>Back</button> : null }
                {(this.state.togl === 1 ) ? <button className="add-test-button" onClick={this.addTest}>Add New Test</button> : <div className="txt">PAYTM</div> }
                <button className="pay-button" onClick={this.getPaymentPage}>{(this.state.togl == 1 ) ? "Payment Status" : "Refresh"}</button>
                {(this.state.togl === 1 ) ?
                    <p>
                        <h1 className="tests-heading">Tests :</h1>
                        <div className="test-options">
                            {this.props.testsList.map(function (test, index) {
                                return <TestOption key={test.id} id={test['id']} showTestDetailsBegin={this.props.showTestDetailsBegin} index={index} showTestDetailsSuccess={this.props.showTestDetailsSuccess} name={test.name} isActive={test.is_active} />
                            }.bind(this))}
                        </div>
                    </p>
                :
                <div>
                <div><Payments cols={cols} data={data} tag={"Transaction History"} len={cols.length}/></div>
                <br/><br/>
                <div><Payments cols={promocols} data={promodata} tag={"Promo Codes"} len={promocols.length}/></div>
                </div>
                }
            </div>
        );
    }
}

TestsList.propTypes = {
    testsList: PropTypes.array,
    loading: PropTypes.bool,
    showAddTest: PropTypes.func,
    showTestDetailsBegin: PropTypes.func,
    showTestDetailsSuccess: PropTypes.func,
    showTests: PropTypes.func
};

export default TestsList;
