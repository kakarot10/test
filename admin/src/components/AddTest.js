import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class AddTest extends Component {
    constructor(props) {
        super(props);
        this.state = {svl:'IBPS',type:'Mock'};
        this.testStatusActive = this.testStatusActive.bind(this);
        this.testStatusInactive = this.testStatusInactive.bind(this);
        this.addTest = this.addTest.bind(this);
        this.cancelAdd = this.cancelAdd.bind(this);
        this.typeChange = this.typeChange.bind(this);
        this.charChange = this.charChange.bind(this);
    }

    testStatusActive() {
        this.props.activateTest();
    }

    testStatusInactive() {
        this.props.deactivateTest();
    }

    addTest() {
        var testName = $('#addTest').val();
        var typeName = this.state.svl;
        var ch = this.state.type;
        $.ajax({
            url: '/admin/tests',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                name: testName,
                is_active: false,
                type: typeName,
                character: ch
            })
        }).then(() => {
            this.props.loadingTestsBegin();
        }).then(() => {
            $.get('/admin/tests', (tests) => {
                this.props.loadingTestsSuccess(tests);
            });
        });
    }

    cancelAdd() {
        this.props.loadingTestsBegin();
    }

    typeChange(e) {
        this.setState({svl:e.target.value});
    }

    charChange(e) {
        this.setState({type:e.target.value});
    }

    render() {
        return (
            <div className="lightbox">
                <div className="background" onClick={this.cancelAdd}></div>
                <div className="add-test">
                    <h1>Add Test</h1>
                    <input id="addTest" type="text" name="addTest" placeholder="Name"/>
                    <br></br> Test Type  :  
                    <select id="testType" onChange={this.typeChange}>
                        <option value="IBPS">IBPS</option>
                        <option value="CAT">CAT</option>
                        <option value="SBI">SBI</option>
                        <option value="SBI PO MAINS">SBI PO MAINS</option>
                    </select>
                    <br></br>
                          <br></br> Test Character : 
                          <select id="testCharacter" onChange={this.charChange}>
                        <option value="Mock">Mock</option>
                        <option value="Topic">Topic</option>
                    </select>

                          <br></br>
                          <br></br>
                    
                    <button className="add-button" onClick={this.addTest}>Add</button>
                </div>
            </div>
        );
    }
}

AddTest.propTypes = {
    testStatus: PropTypes.bool,
    activateTest: PropTypes.func.isRequired,
    deactivateTest: PropTypes.func.isRequired,
    loadingTestsBegin: PropTypes.func.isRequired,
    loadingTestsSuccess: PropTypes.func
};

export default AddTest;
