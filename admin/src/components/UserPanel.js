import React, { Component } from 'react';
import $ from 'jquery';

//var mixpanel = require('mixpanel-browser');
//mixpanel.init("86e70fa37444d5ab81c1ea058ae2fcd4");

class UserPanel extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
    }

    login(e) {
        e.preventDefault();
	var email = $('#email').val();
//	mixpanel.identify(email);
//	mixpanel.track("User Logged in");
        var password = $('#password').val();
        var email = $('#email').val();
        var password = $('#password').val();
        $('#login-form')[0].reset();
        $.ajax({
            url: 'http://localhost:5000/admin-login',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email,
                password
            })
        }).then(function(data) {
            if(data.hasOwnProperty('invalid')) {
                if(data.invalid === 'Email') {
                    $('#user-panel-validator').html("Sorry, Beatest doesn't recognize that email.");
                } else if (data.invalid === 'Password') {
                    $('#user-panel-validator').html("Wrong password. Try again.");
                }
            } else if (data.hasOwnProperty('user')) {
                this.props.login(data.user);
                this.props.toggleLoginPanelVisibility();
//                location.reload();
            }
        }.bind(this));
    }

    render() {
        var userPanelClassName = 'user-panel ' + this.props.loginPanelVisibility;
        return (
            <div>
            <div className={userPanelClassName}>
                <div className="black-background"></div>
                <div className="panel">
                    <form id="login-form" onSubmit={this.login}>
                        <input id="email" className="user-panel-input" type="email" placeholder="Email" required />
                        <input id="password" className="user-panel-input" type="password" placeholder="Password" required />
                        <div><label id="user-panel-validator"></label></div>
                        <button className="user-panel-button">Login</button>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}

export default UserPanel;
