import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import logo from '../../public/logo.png';

class Navlink extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.getTests = this.getTests.bind(this);
    }

    getTests() {
        this.props.loadingTestsBegin();
        $.get('/admin/tests', (tests) => {
            this.props.loadingTestsSuccess(tests);
        });
    }

    render() {
        return (
            <div className="nav">
                <img alt="BEATEST" src={logo} />
                <a onClick={this.getTests}><li>TESTS</li></a>
            </div>
        );
    }
}

Navlink.propTypes = {
    loadingTestsBegin: PropTypes.func,
    loadingTestsSuccess: PropTypes.func
};

export default Navlink;
