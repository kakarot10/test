import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import Questions from './Questions';

class SectionDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.backToSectionsList = this.backToSectionsList.bind(this);
        this.saveSection = this.saveSection.bind(this);
        this.deleteSection = this.deleteSection.bind(this);
        this.showEditQuestions = this.showEditQuestions.bind(this);
    }

    backToSectionsList() {
        this.props.showEditSection();
    }

    saveSection() {
        var sectionName = $('#editSectionName').val();
        $.ajax({
            url: '/admin/tests/' + this.props.testGettingEdited.id
                    + '/sections/' + this.props.sectionGettingEdited.id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                name: sectionName
            })
        }).then(() => {
            this.props.changeSectionName(sectionName);
            this.props.showSectionDetails();
        });
    }

    deleteSection() {
		var pass = prompt("Please enter the password");
		if(pass == "Beatest"){
            $.ajax({
                url: '/admin/tests/'  + this.props.testGettingEdited.id
                        + '/sections/' + this.props.sectionGettingEdited.id,
                type: 'DELETE'
            }).then(() => {
                this.props.loadingSectionsBegin();
            }).then(() => {
                $.get('/admin/tests/' + this.props.testGettingEdited.id
                        + '/sections', (sections) => {
                    this.props.loadingSectionsSuccess(sections);
                });
                this.props.showEditSection();
            });
		}
		else{
			alert("enter the correct password");
		}
    }

    showEditQuestions() {
        $.get('/admin/tests/'  + this.props.testGettingEdited.id
                + '/sections/' + this.props.sectionGettingEdited.id + '/questions', (questions) => {
            this.props.addQuestions(questions);
        }).then(() => {
            this.props.showEditQuestions();
        });
    }

    render() {
        return (
            <div className="test-details">
                {(() => {
                    if(!this.props.editQuestions) {
                        return <div>
                            <span><div><a className="back" onClick={this.backToSectionsList}>&lt; </a></div><h2> {this.props.sectionGettingEdited.name}</h2></span>
                            <br />
                            Created Date : {this.props.sectionGettingEdited.created_date}
                            <br />
                            Edit Section Name: <input id="editSectionName" type="text" name="editTestName"
                                                defaultValue={this.props.sectionGettingEdited.name}/>
                            <button className="save-button" onClick={this.saveSection}>Save</button>
                            <br />
                            <button className="test-details-button deactivate" onClick={this.deleteSection}>Delete</button>
                            <button className="test-details-button" onClick={this.showEditQuestions}>Edit Questions</button>
                        </div>
                    } else {
                        return <Questions sectionName={this.props.sectionGettingEdited.name}
                                currentViewInQuestions={this.props.currentViewInQuestions}
                                editQuestion={this.props.editQuestion}
                                showEditQuestions={this.showEditQuestions}
                                showSectionDetails={this.props.showSectionDetails}
                                questionsList={this.props.questionsList}
                                changeQuestionHtml={this.props.changeQuestionHtml}
                                questionHtml={this.props.questionHtml}
                                addChoice={this.props.addChoice}
                                choices={this.props.choices}
                                testId={this.props.testGettingEdited.id}
                                sectionId={this.props.sectionGettingEdited.id}
                                noOfChoices={this.props.noOfChoices}
                                incrementNoOfChoices={this.props.incrementNoOfChoices} />
                    }
                })()}
            </div>
        );
    }
}

SectionDetails.propTypes = {
    addQuestions: PropTypes.func,
    questionsList: PropTypes.array,
    showEditQuestions: PropTypes.func,
    editQuestions: PropTypes.bool,
    showEditSection: PropTypes.func,
    currentSectionIndex: PropTypes.number,
    loadingSectionsBegin: PropTypes.func,
    loadingSectionsSuccess: PropTypes.func,
    currentViewInQuestions: PropTypes.string,
    choices: PropTypes.array
};

export default SectionDetails;
