import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import AddSection from './AddSection';
import EditTestPage from './EditTestPage';
import EditSectionPage from './EditSectionPage';
import SectionDetails from './SectionDetails';

class EditTest extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.showEditTest = this.showEditTest.bind(this);
        this.showEditSection = this.showEditSection.bind(this);
    }

    showEditTest() {
        this.props.showEditTest();
    }

    showEditSection() {
        this.props.loadingSectionsBegin();
        $.get('/admin/tests/' + this.props.testGettingEdited.id + '/sections', (sections) => {
            this.props.loadingSectionsSuccess(sections);
        });
        this.props.showEditSection();
    }

    render() {
        var navClassName = 'edit-nav-buttons';
        var classForTestView = this.props.currentViewInEditTest === 'EDIT_TEST'? ' active' : '';
        var classForSectionView = this.props.currentViewInEditTest === 'EDIT_SECTION'? ' active' : '';
        var bodyClassName = 'show-body';
        return (
            <div className="edit">
                {(() => {
                    if(this.props.currentViewInEditTest !== 'SECTION_DETAILS') {
                        return <div>
                            <div className="edit-test">
                                <div className="edit-nav">
                                    <a className="back" onClick={this.props.showTestDetails}>&lt; </a>
                                    <div className={navClassName + classForTestView} onClick={this.showEditTest}>Edit Test</div>
                                    <div className={navClassName + classForSectionView} onClick={this.showEditSection}>Edit Sections</div>
                                </div>
                                {(() => {
                                    if(this.props.currentViewInEditTest === 'EDIT_TEST') {
                                        return <EditTestPage testGettingEdited={this.props.testGettingEdited}
                                                class={bodyClassName + classForTestView}
                                                showTestDetails={this.props.showTestDetails}
                                                changeTestName={this.props.changeTestName} />
                                    } else if (this.props.currentViewInEditTest === 'EDIT_SECTION') {
                                        return <EditSectionPage sectionsList={this.props.sectionsList}
                                                class={bodyClassName + classForSectionView}
                                                testGettingEdited={this.props.testGettingEdited}
                                                showAddSection={this.props.showAddSection}
                                                showSectionDetailsBegin={this.props.showSectionDetailsBegin}
                                                showSectionDetailsSuccess={this.props.showSectionDetailsSuccess} />
                                    }
                                })()}
                            </div>
                            {(() => {
                                if(this.props.currentViewInEditSection === 'ADD_SECTION') {
                                return <AddSection loadingSectionsBegin={this.props.loadingSectionsBegin}
                                        loadingSectionsSuccess={this.props.loadingSectionsSuccess}
                                        testGettingEdited={this.props.testGettingEdited} />
                            }})()}
                        </div>
                    } else if (this.props.currentViewInEditTest === 'SECTION_DETAILS') {
                        return <SectionDetails currentSectionIndex={this.props.currentSectionIndex}
                                changeSectionName={this.props.changeSectionName}
                                showEditSection={this.props.showEditSection}
                                showSectionDetails={this.props.showSectionDetails}
                                testGettingEdited={this.props.testGettingEdited}
                                sectionGettingEdited={this.props.sectionGettingEdited}
                                loadingSectionsBegin={this.props.loadingSectionsBegin}
                                loadingSectionsSuccess={this.props.loadingSectionsSuccess}
                                editQuestions={this.props.editQuestions}
                                showEditQuestions={this.props.showEditQuestions}
                                addQuestions={this.props.addQuestions}
                                questionsList={this.props.questionsList}
                                currentViewInQuestions={this.props.currentViewInQuestions}
                                editQuestion={this.props.editQuestion}
                                changeQuestionHtml={this.props.changeQuestionHtml}
                                questionHtml={this.props.questionHtml}
                                addChoice={this.props.addChoice}
                                choices={this.props.choices}
                                noOfChoices={this.props.noOfChoices}
                                incrementNoOfChoices={this.props.incrementNoOfChoices} />
                    }
                })()}
            </div>
        );
    }
}

EditTest.propTypes = {
    questionsList: PropTypes.array,
    addQuestions: PropTypes.func,
    testGettingEdited: PropTypes.object,
    currentViewInEditTest: PropTypes.string,
    currentViewInEditSection: PropTypes.string,
    currentViewInQuestions: PropTypes.string,
    sectionsList: PropTypes.array,
    showEditSection: PropTypes.func,
    showEditTest: PropTypes.func,
    showTestDetailsBegin: PropTypes.func,
    showTestDetailsSuccess: PropTypes.func,
    backToTestDetails: PropTypes.func,
    showTestDetails: PropTypes.func,
    changeTestName: PropTypes.func,
    loadingSectionsBegin: PropTypes.func,
    loadingSectionsSuccess: PropTypes.func,
    showAddSection: PropTypes.func,
    sectionGettingEdited: PropTypes.object,
    currentSectionIndex: PropTypes.number,
    showEditQuestions: PropTypes.func,
    choices: PropTypes.array
};

export default EditTest;
