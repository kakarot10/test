import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class AddSection extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.addSection = this.addSection.bind(this);
        this.cancelAdd = this.cancelAdd.bind(this);
    }

    addSection() {
        var sectionName = $('#addSection').val();
        var sectionTime = $('#addTime').val();
        $.ajax({
            url: '/admin/tests/' + this.props.testGettingEdited.id + '/sections',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                name: sectionName,
				test_id: '',
				total_time: sectionTime
            })
        }).then(() => {
            this.props.loadingSectionsBegin();
        }).then(() => {
            $.get('/admin/tests/' + this.props.testGettingEdited.id + '/sections', (sections) => {
                this.props.loadingSectionsSuccess(sections);
            });
        });
    }

    cancelAdd() {
        this.props.loadingSectionsBegin();
    }

    render() {
        return (
            <div className="lightbox">
                <div className="background" onClick={this.cancelAdd}></div>
                <div className="add-test">
                    <h1>Add Section</h1> Name : 
                    <input id="addSection" type="text" name="addTest" placeholder="Name"/>
                    <br/><br/> Time : 
                    <input id="addTime" type="text" name="addTime" placeholder="Section Time"/>
                    <br/><br/>
                    <button className="add-button" onClick={this.addSection}>Add</button>
                </div>
            </div>
        );
    }
}

AddSection.propTypes = {
    loadingSectionsBegin: PropTypes.func.isRequired,
    loadingSectionsSuccess: PropTypes.func.isRequired
};

export default AddSection;
