import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import TinyMCE from 'react-tinymce';
import Questions from './Questions';
import AddQuestion from './AddQuestion';
import AddTest from './AddTest';

class QuestionOption extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.deleteQuestion = this.deleteQuestion.bind(this);
        this.editQuestion = this.editQuestion.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    deleteQuestion() {
		var pass = prompt("Please enter the password");
		if(pass == "Beatest"){
            $.ajax({
                url: '/admin/questions/' + this.props.question.id,
                type: 'DELETE'
            }).then(() => {
                this.props.showEditQuestions();
            });
		}
		else{
			alert("enter the correct password");
		}
    }
	
	handleChange(e) {
        this.setState({svl:e.target.value});
    }
	
	editQuestion(){
		this.props.editQuestion();
        var ques = this.props.question;
		var iid  = ques.id;
		iid = iid.toString();
		setTimeout(function(){
			$('#hidden-id').val(iid);
			if(ques.type == "TITA"){
				$("#tita").trigger('click');
			}
			if(ques.type == "MCQ"){
				$("#mcq").trigger('click');
			}
			if(ques.type == "RC"){
				$("#rc").trigger('click');
			}
			if(ques.lod == "Easy"){
				$("#Easy").trigger('click');
			}
			if(ques.lod == "Medium"){
				$("#Medium").trigger('click');
			}
			if(ques.lod == "Difficult"){
				$("#Difficult").trigger('click');
			}
			//$("#lod").val(ques.lod);
			//$("#cat").trigger('click');
			$("#topic").val(ques.topic);
			window.parent.tinymce.get('question_html').setContent(ques.html);
			window.parent.tinymce.get('logic_html').setContent(ques.logic);
			$('#points_correct').val(ques.points_correct);
			$('#points_wrong').val(ques.points_wrong);
			if(ques.type == 'TITA'){
				window.parent.tinymce.editors[2].setContent(ques.tita_answer);			
			}
			else if(ques.type == 'MCQ'){
				setTimeout(function(){
					for(var i=1;i<ques.choices.length;i++){
					$("#inc-choices").trigger('click'); 
				}
					setTimeout(function(){
						for(var i=1;i<=ques.choices.length;i++){
						window.parent.tinymce.editors[i+1].setContent(ques.choices[i-1].html);
						if(ques.choices[i-1].is_correct == true){
							$('#correctAns').val(i);
						}
					}
					},1000);
				},1000);
			}
			else if(ques.type == 'RC'){
				window.parent.tinymce.get('rc_html').setContent(ques.rc_passage);
					setTimeout(function(){
					for(var i=1;i<ques.choices.length;i++){
					$("#inc-choices").trigger('click');
				}
				setTimeout(function(){
						for(var i=2;i<=ques.choices.length+1;i++){
						window.parent.tinymce.editors[i+1].setContent(ques.choices[i-2].html);
						if(ques.choices[i-2].is_correct == true){
							$('#correctAns').val(i-1);
						}
					}
					},1000);
				},1000);	
			}
		},1000);
		
	}

    render() {
        return (
            <li className="questions-option" id={this.props.question.id}>
                <span dangerouslySetInnerHTML={{__html: this.props.question.html}}></span>
                <button className="edit button" onClick={this.editQuestion}>Edit</button>
                <button className="deactivate button" onClick={this.deleteQuestion}>Delete</button>
            </li>
        );
    }
}

QuestionOption.propTypes = {
    question: PropTypes.object
}

export default QuestionOption;
