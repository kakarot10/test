import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import TinyMCE from 'react-tinymce';

class AddQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {svl:'TITA',lod:'Easy',topic:'Select',testType:'CAT'};
        this.handleQuestionHtmlChange = this.handleQuestionHtmlChange.bind(this);
        this.handleChoiceHtmlChange = this.handleChoiceHtmlChange.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
        this.incrementChoices = this.incrementChoices.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleLogic = this.handleLogic.bind(this);
        this.handleLod = this.handleLod.bind(this);
        this.handleType = this.handleType.bind(this);
        this.handleTopic = this.handleTopic.bind(this);
        
    }

    handleQuestionHtmlChange = (e) => {
        this.props.changeQuestionHtml(e.target.getContent());
    }

    handleChoiceHtmlChange = (e) => {
        this.props.addChoice(e.target.getContent());
    }

    handleChange(e) {
		var style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = '.blue { background-color: #0e7ee5; }';
		document.getElementsByTagName('head')[0].appendChild(style);
		$("button").removeClass('blue');
		$(e.target).addClass('blue');
        this.setState({svl:e.target.value});
    }

    handleLod(e) {
		var style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = '.red { background-color: #D2B6B2; }';
		document.getElementsByTagName('head')[0].appendChild(style);
		$("button").removeClass('red');
		$(e.target).addClass('red');
        this.setState({lod:e.target.value});
    }

    handleType(e) {
        this.setState({testType:e.target.value});
    }

    handleTopic(e) {
        this.setState({topic:e.target.value});
    }

    addQuestion() {
        var c = '[';
        let is_correct;
        let question = {};
        let data = [];
		question.id = $('#hidden-id').val();
        question.html = window.parent.tinymce.get('question_html').getContent();
        question.points_correct = $('#points_correct').val();
        question.points_correct = parseFloat(question.points_correct);
        question.points_wrong = $('#points_wrong').val();
        question.points_wrong = parseFloat(question.points_wrong);
        question.logic = window.parent.tinymce.get('logic_html').getContent();
        let correctAns = parseInt($('#correctAns').val()); 
        question.type = this.state.svl;
        question.lod = this.state.lod;
		question.topic = $('#topic').val();
        let index;
        let answer;

        if(question.type === 'RC'){
          question.rc_passage = window.parent.tinymce.get('rc_html').getContent();  
        }
        else question.rc_passage = "";  

        for (var i = 0; i < window.parent.tinymce.editors.length; i++){
          var str=window.parent.tinymce.editors[i].getContent();
          var res = str.replace(/"/g, "'");
          var res_1 = res.replace(/\n/g, " ");
          data[i]=res_1;
          if(question.type === 'RC' && data[i] === question.rc_passage){
            index = i;
          }
        }

        if(question.type === 'RC'){
            data.splice(index,1);
        }

        for (var i = 2; i < data.length; i++){
            if(this.state.svl !== 'TITA'){
                if(i === correctAns+1){
                    is_correct = 1;
                    answer = data[i];
                }
                else is_correct = 0;
            }
            else {
                is_correct = 1;
                answer = data[2];
            }
            c += '{';
            c += '"html" : "' + data[i] + '" ,"is_correct" : "' + is_correct + '"';
            c += '},';
        }
        c = c.slice(0, -1);
        c += ']';
        c = JSON.parse(c);
        question.choices = c;


        if(question.type === 'TITA'){
            answer = answer.replace(/\D/g, '');
            question.tita_answer = parseInt(answer);
        }

        let questions = [question];
        $.ajax({
            url: '/admin/tests/'  + this.props.testId
                    + '/sections/' + this.props.sectionId + '/questions',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(
                questions
            )
        }).then(() => {
            this.props.showEditQuestions();
        });
    }

    incrementChoices() {
        if(this.state.svl !== 'TITA')
            this.props.incrementNoOfChoices(this.props.noOfChoices);
        else alert('TITA TYPE !! Only 1 choice !!');
    }

    handleLogic = (e) => {
        //alert(e);
    }

    render() {
        var choices = [];
        for (var i = 0; i < this.props.noOfChoices; i++) {
            choices.push(
                <div key={i}>OPTION
                    <div className="choice-no">{i+1}</div>
                    <TinyMCE className="choice-html"
                        config={{
                            plugins: 'link code image media charmap imagetools paste',
                            paste_auto_cleanup_on_paste : true,
                            paste_remove_styles: true,
                            paste_remove_styles_if_webkit: true,
                            paste_strip_class_attributes: true,
                            paste_as_text : true,
                            menubar: '',
                            file_picker_types: 'image',
                            toolbar: 'bold italic | link image | charmap',
                            file_picker_callback: function(callback, value, meta) {
                                if (meta.filetype === 'image') {
                                    $('#upload').trigger('click');
                                    $('#upload').on('change', function() {
                                        var file = this.files[0];
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            callback(e.target.result, {
                                                alt: ''
                                            });
                                        };
                                        reader.readAsDataURL(file);
                                    });
                                }
                            }
                        }}
                        onSubmit={this.handleChoiceHtmlChange} />
                    <input name="image" type="file" id="upload" className="hide" onchange=""/>
                </div>
            );
        }
        return (
            <div>
                <div>
                    <a className="back" onClick={this.props.showEditQuestions}>&lt; </a>
                    <br/><br/>
                </div>
                <div className="add-question">
				Question number : 
				<input type="text" id="hidden-id" disabled="true"/>
				<br/><br/>
                    Question Type:
                        <button id="tita" value="TITA" onClick={this.handleChange}>TITA</button>
                        <button id="mcq" value="MCQ" onClick={this.handleChange}>MCQ</button>
						<button id="rc" value="RC" onClick={this.handleChange}>RC</button>
					&emsp;
                    LOD : 
                        <button id="Easy" value="Easy" onClick={this.handleLod}>Easy</button>
                        <button id="Medium" value="Medium" onClick={this.handleLod}>Medium</button>
                        <button id="Difficult" value="Difficult" onClick={this.handleLod}>Difficult</button>
                    &emsp;
                    Topic :
                        <select id="topic" onChange={this.handleTopic}>
                        <option value="">Select</option>
                        <option value="Percentage">Percentage</option>
                        <option value="Profit/Loss/Discount">Profit/Loss/Discount</option>
						<option value="Compound & Simple Interest">Compound & Simple Interest</option>
						<option value="Time,Speed,Distance & Work">Time,Speed,Distance & Work</option>
						<option value="Ratio and Proportion">Ratio and Proportion</option>
						<option value="Mixtures and Alligation">Mixtures and Alligation</option>
						<option value="Number System">Number System</option>
						<option value="Algebra">Algebra</option>
						<option value="Geometry">Geometry</option>
                        <option value="Mensuration">Mensuration</option>
						<option value="Permutation & Combination">Permutation & Combination</option>
                        <option value="Probability">Probability</option>
						<option value="Reading Comprehension">Reading Comprehension</option>
						<option value="Sentence Correction">Sentence Correction</option>
						<option value="Parajumbles">Parajumbles</option>
						<option value="Para-Completion">Para-Completion</option>
						<option value="Summary Based Question">Summary Based Question</option>
						<option value="Verbal Ability">Verbal Ability</option>
						<option value="Verbal Reasoning">Verbal Reasoning</option>
						<option value="Odd one out">Odd one out</option>
						<option value="Mensuration">Mensuration</option>
                       	<option value="Puzzles">Puzzles</option>
						<option value="Syllogisms">Syllogisms</option>
						<option value="Input Output">Input Output</option>
						<option value="Coding Decoding">Coding Decoding</option>
						<option value="Miscellaneous">Miscellaneous</option>
						<option value="Inequalities">Inequalities</option>
						<option value="RC">RC</option>
						<option value="Cloze Test">Cloze Test</option>
						<option value="Fill in">Fill in</option>
						<option value="Sentence error correction">Sentence error correction</option>
						<option value="Tables">Tables</option>
						<option value="Bar Graph">Bar Graph</option>
						<option value="Line Graph">Line Graph</option>
						<option value="Pie Charts">Pie Charts</option>
						<option value="Simplifications">Simplifications</option>
                        <option value="Quadratic Equation">Quadratic Equation</option>
						<option value="Seating Arrangement">Seating Arrangement</option>
                        <option value="Number Series">Number Series</option>
                    </select>   
                                    
                    <br/><br/>
                    QUESTION :
                    <br/>                   
                    <TinyMCE id="question_html"
                        config={{
                            plugins: 'link code image media charmap imagetools paste',
                            paste_auto_cleanup_on_paste : true,
                            paste_remove_styles: true,
                            paste_remove_styles_if_webkit: true,
                            paste_strip_class_attributes: true,
                            paste_as_text : true,
                            menubar: '',
                            file_picker_types: 'image',
                            toolbar: 'bold italic | link image | charmap',
                            file_picker_callback: function(callback, value, meta) {
                                if (meta.filetype === 'image') {
                                    $('#upload').trigger('click');
                                    $('#upload').on('change', function() {
                                        var file = this.files[0];
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            callback(e.target.result, {
                                                alt: ''
                                            });
                                        };
                                        reader.readAsDataURL(file);
                                    });
                                }
                            }
                        }}
                        onSubmit={this.handleQuestionHtmlChange} />
                    <input name="image" type="file" id="upload" className="hide" onchange=""/>
                    Points Correct: <input id="points_correct" type="text" defaultValue="0" />&nbsp;
                    Points Wrong: <input id="points_wrong" type="text" defaultValue="0" />
                    <br/><br/> LOGIC : 
                    <TinyMCE id="logic_html"
                        config={{
                            plugins: 'link code image media charmap imagetools paste',
                            paste_auto_cleanup_on_paste : true,
                            paste_remove_styles: true,
                            paste_remove_styles_if_webkit: true,
                            paste_strip_class_attributes: true,
                            paste_as_text : true,
                            menubar: '',
                            file_picker_types: 'image',
                            toolbar: 'bold italic | link image | charmap',
                            file_picker_callback: function(callback, value, meta) {
                                if (meta.filetype === 'image') {
                                    $('#upload').trigger('click');
                                    $('#upload').on('change', function() {
                                        var file = this.files[0];
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            callback(e.target.result, {
                                                alt: ''
                                            });
                                        };
                                        reader.readAsDataURL(file);
                                    });
                                }
                            }
                        }}
                        onSubmit={this.handleLogic} />
                </div>
                {(this.state.svl === 'RC')?<p>
                ENTER RC PASSAGE : 
                <TinyMCE id="rc_html"
                        config={{
                            plugins: 'link code image media charmap imagetools paste',
                            paste_auto_cleanup_on_paste : true,
                            paste_remove_styles: true,
                            paste_remove_styles_if_webkit: true,
                            paste_strip_class_attributes: true,
                            paste_as_text : true,
                            menubar: '',
                            file_picker_types: 'image',
                            toolbar: 'bold italic | link image | charmap',
                            file_picker_callback: function(callback, value, meta) {
                                if (meta.filetype === 'image') {
                                    $('#upload').trigger('click');
                                    $('#upload').on('change', function() {
                                        var file = this.files[0];
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            callback(e.target.result, {
                                                alt: ''
                                            });
                                        };
                                        reader.readAsDataURL(file);
                                    });
                                }
                            }
                        }}
                        onSubmit={this.handleLogic}  /> </p>
                : null}      


                <div className="choices">
                    {(this.state.svl === 'TITA')? "ENTER NUMERICAL ANSWER" : "Choices :"}
                    <form>{choices}</form>
                    {(this.state.svl !== 'TITA')? <button className="button" onClick={this.incrementChoices} id="inc-choices" >Add choice</button>: null}
                </div>
                <br/><br/>{(this.state.svl !== 'TITA')? "Correct Option Number : ": null}
                {(this.state.svl !== 'TITA')?<input id="correctAns" type="text" name="correctAns" placeholder="Correct Option Number" />: null}      
                <br/><br/>
                <button className="button add-question" onClick={this.addQuestion}>Submit</button>
            </div>
        );
    }
}

AddQuestion.propTypes = {
    questionHtml: PropTypes.string,
    choiceHtml: PropTypes.string,
    choices: PropTypes.array,
    noOfChoices: PropTypes.number
}

export default AddQuestion;
