import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class Payments extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.generateHeaders = this.generateHeaders.bind(this);
        this.generateRows = this.generateRows.bind(this);
        this.generateTag = this.generateTag.bind(this);
    
    }    

    generateHeaders() {
        var cols = this.props.cols;
        return cols.map(function(colData) {
            return <th key={colData.key}> {colData.label} </th>;
        });
    }

    generateRows() {
        var cols = this.props.cols,
            data = this.props.data;

        return data.map(function(item) {
            var cells = cols.map(function(colData) {
                return <td> {item[colData.key]} </td>;
            });
            return <tr className="pay-key" key={item.id}> {cells} </tr>;
        });
    }

    generateTag() {
        var tag = this.props.tag,
            len = this.props.len;

        return <th className="pay-key" colSpan={len}> {tag} </th>;

    }

    render() {
        var headerComponents = this.generateHeaders(),
            rowComponents = this.generateRows(),
            tagComponents = this.generateTag();

        return (
            <table className="pay-table">
                <thead> {tagComponents} </thead>
                <thead> {headerComponents} </thead> 
                <tbody> {rowComponents} </tbody>
            </table>
        );
    }
}


export default Payments;
