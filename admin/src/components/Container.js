import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import * as action from '../actions/action';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Navlink from './Navlink';
import TestsList from './TestsList';
import TestDetails from './TestDetails';
import AddTest from './AddTest';
import EditTest from './EditTest';
import UserPanel from './UserPanel';

const initialState = {
    user: null,
    loginPanelVisibility: 'block',
    currentView: 'TESTS_LIST',
    testsList: [],
    sectionsList: [],
    questionsList: [],
    testGettingEdited: {},
    currentViewInEditTest: 'EDIT_TEST',
    currentViewInEditSection: '',
    userId: 1,
    editQuestions: false
}

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.props.actions.loadingTestsBegin();
        $.get('/admin/tests', (tests) => {
            this.props.actions.loadingTestsSuccess(tests);
        });
    }

    render() {
            return (
                <div className="container">
                    <Navlink />
                    {(() => {
                    if (this.props.currentView === 'TESTS_LIST') {
                        if(this.props.loginPanelVisibility === 'block'){
                              return <UserPanel loginPanelVisibility={this.props.loginPanelVisibility}
                                    toggleLoginPanelVisibility={this.props.actions.toggleLoginPanelVisibility}
                                    login={this.props.actions.login} />
                        }
                         else {
                              return <TestsList testsList={this.props.testsList}
                                    loading={this.props.isTestsLoading}
                                    showTestDetailsBegin={this.props.actions.showTestDetailsBegin}
                                    showTestDetailsSuccess={this.props.actions.showTestDetailsSuccess}
                                    showAddTest={this.props.actions.showAddTest} />
                         }
                    } else if (this.props.currentView === 'TEST_DETAILS') {
                        return <TestDetails testsList={this.props.testsList}
                                testGettingEdited={this.props.testGettingEdited}
                                showTests={this.props.actions.showTests}
                                changeTestStatus={this.props.actions.changeTestStatus}
                                showEditTest={this.props.actions.showEditTest}
                                currentTestIndex={this.props.currentTestIndex}
                                loadingTestsBegin={this.props.actions.loadingTestsBegin}
                                loadingTestsSuccess={this.props.actions.loadingTestsSuccess} />
                    } else if (this.props.currentView === 'ADD_TEST') {
                        return <div>
                                <TestsList testsList={this.props.testsList}
                                    loading={this.props.isTestsLoading} />
                                <AddTest testStatus={this.props.testStatus}
                                    activateTest={this.props.actions.activateTest}
                                    deactivateTest={this.props.actions.deactivateTest}
                                    loadingTestsBegin={this.props.actions.loadingTestsBegin}
                                    loadingTestsSuccess={this.props.actions.loadingTestsSuccess} />
                        </div>
                    } else if (this.props.currentView === 'EDIT_TEST') {
                        return <EditTest currentViewInEditTest={this.props.currentViewInEditTest}
                                    currentViewInEditSection={this.props.currentViewInEditSection}
                                    showEditTest={this.props.actions.showEditTest}
                                    currentTestIndex={this.props.currentTestIndex}
                                    showTestDetails={this.props.actions.showTestDetails}
                                    showEditSection={this.props.actions.showEditSection}
                                    showTestDetailsBegin={this.props.actions.showTestDetailsBegin}
                                    showTestDetailsSuccess={this.props.actions.showTestDetailsSuccess}
                                    testGettingEdited={this.props.testGettingEdited}
                                    changeTestName={this.props.actions.changeTestName}
                                    loadingSectionsBegin={this.props.actions.loadingSectionsBegin}
                                    loadingSectionsSuccess={this.props.actions.loadingSectionsSuccess}
                                    sectionsList={this.props.sectionsList} editQuestions={this.props.editQuestions}
                                    showAddSection={this.props.actions.showAddSection}
                                    showSectionDetailsBegin={this.props.actions.showSectionDetailsBegin}
                                    showSectionDetailsSuccess={this.props.actions.showSectionDetailsSuccess}
                                    sectionGettingEdited={this.props.sectionGettingEdited}
                                    currentSectionIndex={this.props.currentSectionIndex}
                                    changeSectionName={this.props.actions.changeSectionName}
                                    showSectionDetails={this.props.actions.showSectionDetails}
                                    showEditQuestions={this.props.actions.showEditQuestions}
                                    addQuestions={this.props.actions.addQuestions}
                                    questionsList={this.props.questionsList}
                                    currentViewInQuestions={this.props.currentViewInQuestions}
                                    editQuestion={this.props.actions.editQuestion}
                                    changeQuestionHtml={this.props.actions.changeQuestionHtml}
                                    questionHtml={this.props.questionHtml}
                                    addChoice={this.props.actions.addChoice}
                                    choices={this.props.choices}
                                    noOfChoices={this.props.noOfChoices}
                                    incrementNoOfChoices={this.props.actions.incrementNoOfChoices} />
                    }})()}
                </div>
            );
    }
}

Container.propTypes = {
    isTestsLoading: PropTypes.bool,
    isSectionsLoading: PropTypes.bool,
    loginPanelVisibility: PropTypes.string,
    user: PropTypes.object,
    currentView: PropTypes.string,
    testsList: PropTypes.array,
    questionsList: PropTypes.array,
    userId: PropTypes.number,
    sectionsList: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    testGettingEdited: PropTypes.object.isRequired,
    sectionGettingEdited: PropTypes.object,
    currentViewInEditTest: PropTypes.string,
    currentViewInEditSection: PropTypes.string,
    currentViewInQuestions: PropTypes.string,
    currentTestIndex: PropTypes.number,
    currentSectionIndex: PropTypes.number,
    editQuestions: PropTypes.bool,
    questionHtml: PropTypes.string,
    choices: PropTypes.array,
    noOfChoices: PropTypes.number
};

function mapStateToProps(state, props) {
    return {
        isTestsLoading: state.isTestsLoading,
        isSectionsLoading: state.isSectionsLoading,
        currentView: state.currentView,
        testsList: state.testsList,
        questionsList: state.questionsList,
        userId: state.userId,
        sectionsList: state.sectionsList,
        testGettingEdited: state.testGettingEdited,
        sectionGettingEdited: state.sectionGettingEdited,
        currentViewInEditTest: state.currentViewInEditTest,
        currentViewInEditSection: state.currentViewInEditSection,
        currentViewInQuestions: state.currentViewInQuestions,
        currentTestIndex: state.currentTestIndex,
        currentSectionIndex: state.currentSectionIndex,
        editQuestions: state.editQuestions,
        questionHtml: state.questionHtml,
        loginPanelVisibility: state.loginPanelVisibility,
        user: state.user,
        choices: state.choices,
        noOfChoices: state.noOfChoices
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);
