import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class EditTestPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.saveTest = this.saveTest.bind(this);
    }

    saveTest() {
        var testName = $('#editTestName').val();
        $.ajax({
            url: '/admin/tests/' + this.props.testGettingEdited.id,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                name: testName
            })
        }).then(() => {
            this.props.changeTestName(testName);
            this.props.showTestDetails();
        });
    }

    render() {
        return (
            <div className={this.props.class}>
                Edit Test Name: <input id="editTestName" type="text" name="editTestName"
                                    defaultValue={this.props.testGettingEdited.name}/>
                <button className="save-button" onClick={this.saveTest}>Save</button>
            </div>
        );
    }
}

EditTestPage.propTypes = {
    testGettingEdited: PropTypes.object,
    showTestDetails: PropTypes.func,
    changeTestName: PropTypes.func,
};

export default EditTestPage;
