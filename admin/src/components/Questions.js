import React, {PropTypes, Component} from 'react';
import $ from 'jquery';
import QuestionOption from './QuestionOption';
import AddQuestion from './AddQuestion';

class Questions extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.backToSectionDetails = this.backToSectionDetails.bind(this);
        this.editQuestion = this.editQuestion.bind(this);
    }

    backToSectionDetails() {
        this.props.showSectionDetails();
    }

    editQuestion() {
        this.props.editQuestion();
    }

    render() {
        return (
            <div className="questions-list">
                {(() => {
                    if (this.props.currentViewInQuestions === 'EDIT_QUESTION') {
                        return <div>
                            <AddQuestion changeQuestionHtml={this.props.changeQuestionHtml}
                            questionHtml={this.props.questionHtml}
                            addChoice={this.props.addChoice}
                            choices={this.props.choices} choiceHtml=""
                            testId={this.props.testId}
                            sectionId={this.props.sectionId}
                            noOfChoices={this.props.noOfChoices}
							showEditQuestions={this.props.showEditQuestions}
                            incrementNoOfChoices={this.props.incrementNoOfChoices} />
                        </div>
                    } else if(this.props.currentViewInQuestions === 'SHOW_QUESTIONS') {
                        return <div>
                            <div>
                                <a className="back" onClick={this.backToSectionDetails}>&lt; </a>
                            </div>
                            <h2>
                                {this.props.sectionName}
								<br/>
								Add Questions
								<br/>
								<button className="button" onClick={this.editQuestion}>Add Question</button>
								<br/>
                            </h2>
                            <ol className="question">
                                {this.props.questionsList.map(function (question, index) {
                                    return <QuestionOption key={question.id} id={question.id} index={index} testId={this.props.testId}  sectionId={this.props.sectionId} question={question} editQuestion={this.editQuestion} showEditQuestions={this.props.showEditQuestions}/>
                                }.bind(this))}
                            </ol>
                        </div>
                    }
                })()}
            </div>
        );
    }
}

Questions.propTypes = {
    currentViewInQuestions: PropTypes.string,
    questionsList: PropTypes.array,
    choices: PropTypes.array
}

export default Questions;
