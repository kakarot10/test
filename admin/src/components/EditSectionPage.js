import React, {PropTypes, Component} from 'react';
import SectionOption from './SectionOption';

class EditSectionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.showAddSection = this.showAddSection.bind(this);
    }

    showAddSection() {
        this.props.showAddSection();
    }

    render() {
        return (
            <div className="edit-section-page">
                <div className={this.props.class}>
                    {this.props.sectionsList.map(function (section, index) {
                        return <SectionOption key={section.id} id={section['id']}
                                name={section.name} index={index}
                                testId={this.props.testGettingEdited.id}
                                showSectionDetailsBegin={this.props.showSectionDetailsBegin}
                                showSectionDetailsSuccess={this.props.showSectionDetailsSuccess} />
                    }.bind(this))}
                </div>
                <button className="add-test-button" onClick={this.showAddSection}>Add New Section</button>
            </div>
        );
    }
}

EditSectionPage.propTypes = {
    class: PropTypes.string,
    showAddSection: PropTypes.func,
    testGettingEdited: PropTypes.object,
    showTestDetails: PropTypes.func,
    changeTestName: PropTypes.func,
    showSectionDetailsBegin: PropTypes.func,
    showSectionDetailsSuccess: PropTypes.func
};

export default EditSectionPage;
