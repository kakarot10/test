import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class SectionOption extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.showSectionDetailsBegin(this.props.id);
        $.get('/admin/tests/' + this.props.testId + '/sections/' + this.props.id, (section) => {
            this.props.showSectionDetailsSuccess(this.props.index, section);
        });
    }

    render() {
        return (
            <div className='test-option' onClick={this.handleClick}>{this.props.name}</div>
        );
    }
}

SectionOption.propTypes = {
    testId: PropTypes.number.isRequired,
    index: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    showSectionDetailsBegin: PropTypes.func,
    showSectionDetailsSuccess: PropTypes.func
};

export default SectionOption;
