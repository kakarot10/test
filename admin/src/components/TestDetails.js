import React, {PropTypes, Component} from 'react';
import $ from 'jquery';

class TestDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.editTest = this.editTest.bind(this);
        this.activateTest = this.activateTest.bind(this);
        this.deactivateTest = this.deactivateTest.bind(this);
        this.deleteTest = this.deleteTest.bind(this);
        this.backToTestsList = this.backToTestsList.bind(this);
    }

    editTest() {
        this.props.showEditTest();
    }

    activateTest() {
        $.ajax({
            url: '/admin/tests/' + this.props.testGettingEdited.id + '/activate',
            type: 'PUT'
        }).then(() => {
            this.props.changeTestStatus(true);
        });
    }

    deactivateTest() {
        $.ajax({
            url: '/admin/tests/' + this.props.testGettingEdited.id + '/deactivate',
            type: 'PUT'
        }).then(() => {
            this.props.changeTestStatus(false);
        });
    }

    deleteTest() {
		var pass = prompt("Please enter the password");
		if(pass == "Beatest"){
            $.ajax({
                url: '/admin/tests/' + this.props.testGettingEdited.id,
                type: 'DELETE'
            }).then(() => {
                this.props.loadingTestsBegin();
            }).then(() => {
                $.get('/admin/tests', (tests) => {
                    this.props.loadingTestsSuccess(tests);
                });
            });
		}
		else{
			alert("enter the correct password");
		}
    }

    backToTestsList() {
        this.props.showTests();
    }

    render() {
        return (
            <div className="test-details">
                <span><a className="back" onClick={this.backToTestsList}>&lt; </a> {this.props.testGettingEdited.name}</span>
                <br />
                Created Date : {this.props.testGettingEdited.created_date}
                <br />
                Test Status : {this.props.testGettingEdited.is_active? 'Active' : 'Inactive'}
                <br />
                <button className="test-details-button" onClick={this.editTest}>Edit</button>
                {(() => {
                    if (this.props.testGettingEdited.is_active) {
                        return <button className="test-details-button deactivate" onClick={this.deactivateTest}>Deactivate</button>
                    } else {
                        return <button className="test-details-button activate" onClick={this.activateTest}>Activate</button>
                    }
                })()}
                <button className="test-details-button deactivate" onClick={this.deleteTest}>Delete</button>
            </div>
        );
    }
}

TestDetails.propTypes = {
    testsList: PropTypes.array,
    testGettingEdited: PropTypes.object,
    showTests: PropTypes.func,
    changeTestStatus: PropTypes.func.isRequired,
    showEditTest: PropTypes.func.isRequired,
    loadingTestsBegin: PropTypes.func.isRequired,
    loadingTestsSuccess: PropTypes.func.isRequired,
    editTest: PropTypes.func,
    activateTest: PropTypes.func,
    deactivateTest: PropTypes.func
};

export default TestDetails;
